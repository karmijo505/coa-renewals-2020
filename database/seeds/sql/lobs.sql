-- noinspection SqlNoDataSourceInspectionForFile

-- noinspection SqlDialectInspectionForFile

INSERT INTO lobs VALUES ('Accident and Health',1,200);
INSERT INTO lobs VALUES ('Accident and Health (Excluding Health)',2,200);
INSERT INTO lobs VALUES ('Accident and Health (Health Only)',3,200);
INSERT INTO lobs VALUES ('Accident and Health (Vehicle Only)',4,200);
INSERT INTO lobs VALUES ('Casualty',5,200);
INSERT INTO lobs VALUES ('Casualty (Credit Only)',6,200);
INSERT INTO lobs VALUES ('Casualty (Excludes Mortgage Guaranty & Workers Comp)',7,200);
INSERT INTO lobs VALUES ('Casualty (Excluding Workers Comp & Emp. Liability)',8,200);
INSERT INTO lobs VALUES ('Casualty (Liability & Malpractice Only)',9,200);
INSERT INTO lobs VALUES ('Casualty (Malpractice Only)',10,200);
INSERT INTO lobs VALUES ('Casualty (Mechanical Breakdown Only)',11,200);
INSERT INTO lobs VALUES ('Casualty (Mexican Casualty Only)',12,200);
INSERT INTO lobs VALUES ('Casualty (Mortgage Guaranty Only)',13,200);
INSERT INTO lobs VALUES ('Casualty (Pre-Paid Legal Only)',14,200);
INSERT INTO lobs VALUES ('Casualty (Surety Only)',15,200);
INSERT INTO lobs VALUES ('Casualty (Title Only)',16,200);
INSERT INTO lobs VALUES ('Casualty (Vehicle Only)',17,200);
INSERT INTO lobs VALUES ('Casualty (Workers Comp & Employers Liability Only)',18,200);
INSERT INTO lobs VALUES ('Health Maintenance Organization',19,200);
INSERT INTO lobs VALUES ('Life and Annuities',20,200);
INSERT INTO lobs VALUES ('Non-Profit Health Care Plan',21,200);
INSERT INTO lobs VALUES ('Pre-Paid Dental',22,200);
INSERT INTO lobs VALUES ('Property',23,200);
INSERT INTO lobs VALUES ('Property (Excludes Wet Marine & Transportation)',24,200);
INSERT INTO lobs VALUES ('Property (Excluding Marine & Transportation)',25,200);
INSERT INTO lobs VALUES ('Property (Vehicle Only)',26,200);
INSERT INTO lobs VALUES ('Qualified Dental Plan',27,200);
INSERT INTO lobs VALUES ('Qualified Health Plan',28,200);
INSERT INTO lobs VALUES ('Variable Life and Annuity',29,200);
INSERT INTO lobs VALUES ('Fraternal Benefit Society',30,100);
INSERT INTO lobs VALUES ('Annual Continuation of Registration Fee',31,200);
INSERT INTO lobs VALUES ('Liability Risk Retention Group',32,200);
INSERT INTO lobs VALUES ('Motor Club',33,100);
