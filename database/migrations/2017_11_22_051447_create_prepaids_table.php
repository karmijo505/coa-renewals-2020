<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrepaidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prepaids', function (Blueprint $table) {

            $table->string('company_name',255);
            $table->integer('naic_cocode');
            $table->string('line_of_business',255);
            $table->string('mlg_address1',255);
            $table->string('mailing_city',255);
            $table->string('mailing_state',255);
            $table->string('mailing_zip',10);
            $table->integer('lob_id');

            $table->primary('naic_cocode');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prepaid');
        Schema::dropIfExists('prepaids');
    }
}
