const elixir = require('laravel-elixir');

elixir(function(mix) {

    mix.sass('app.scss')
        .copy('./bower_components/materialize/dist/fonts', 'public/fonts');

    mix.scripts([
        './node_modules/jquery/dist/jquery.js',
        './bower_components/typeahead.js/dist/typeahead.bundle.js',
        './bower_components/growl/javascripts/jquery.growl.js',
        './bower_components/materialize/dist/js/materialize.js',
        'app.js'
    ]);
});
