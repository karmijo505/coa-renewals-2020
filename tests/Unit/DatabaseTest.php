<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DatabaseTest extends TestCase
{

    /**
     * Tests Database Tables
     * Companies, Lobs, Company_Lob
     *
     * @return void
     */

    public function testCompaniesTable()
    {

        $this->assertDatabaseHas('companies', [
            'naic_cocode' => '10004'
        ]);
    }

    public function testLobsTable()
    {
        $this->assertDatabaseHas('lobs', [
            'lob_id' => '1'
        ]);

        $this->assertDatabaseHas('lobs', [
            'lob_id' => '3'
        ]);

        $this->assertDatabaseHas('lobs', [
            'lob_id' => '5'
        ]);
    }

    public function testCompany_LobTable()
    {
        $this->assertDatabaseHas('company_lob', [
            'lob_id' => '1'
        ]);

        $this->assertDatabaseHas('company_lob', [
            'naic_cocode' => '95739'
        ]);

        $this->assertDatabaseHas('company_lob', [
            'lob_id' => '5'
        ]);
    }
}
