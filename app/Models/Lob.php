<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lob extends Model
{
    protected $primaryKey = 'lob_id';

    public function companies() {

        return $this->belongsToMany("App\Models\Company", null, "lob_id", "naic_cocode", 'lob_id', 'naic_cocode');
    }
}
