<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $primaryKey = 'naic_cocode';

    public function lobs() {

        return $this->belongsToMany('App\Models\Lob',null , 'naic_cocode', 'lob_id', 'naic_cocode', 'lob_id');
    }
}
